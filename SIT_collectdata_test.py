import pytest
import time
import datetime
import csv
import os.path
from os import path
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

@pytest.fixture(scope="session", autouse=True)
def test_setup():
    global driver, a
    driver = webdriver.Chrome(executable_path="C:/Users/User/PycharmProjects/SPICE-SIT/chromedriver.exe")
    driver.get("https://spice-sit.tm.com.my/#/")
    driver.implicitly_wait(10)
    driver.maximize_window()
    a = ActionChains(driver)

def test_login():

    username = driver.find_element(By.CSS_SELECTOR, '[placeholder="Username"]').send_keys('C00339')
    password = driver.find_element(By.CSS_SELECTOR, '[placeholder="Password"]').send_keys('AbeJun22')
    login_button = driver.find_element_by_xpath("//button[@class='login100-form-btn']").click()

def test_search():

    service_id = driver.find_element_by_xpath("//input[@id='ovalnkid']").send_keys('02377479264')
    search_button = driver.find_element_by_xpath("//button[@id='ovalnkid2']").click()
    time.sleep(15)

def test_collectdata_holisticsummary():

               #PROFILE SECTION
    xpath = { "account_number" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div/div/p[2]",
              "unifi_tv" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div/div[2]/p[2]",
              "segment_code" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div/div[3]/p[2]",
              "plan_subscribe" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div/div[4]/p[2]",
              "difficult_customer" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div/div[5]/p[2]",
              "internet1" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[2]/div/p[2]",
              "voice1" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[2]/div[2]/p[2]",
              "commitment" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[2]/div[3]/p[2]",
              "vvip_customer" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[2]/div[4]/p[2]",
              "customer_level" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[2]/div[5]/p[2]",
              "msisdn1" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[3]/div/p[2]",
              "imsi_number" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[3]/div[2]/p[2]",
              "autopay_status" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[3]/div[3]/p[2]/span",
              "source1" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div/div[3]/div[3]/p[2]/span",
              "installation_address" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-holistic/div/div/div/div/div[3]/div/div[2]/p[2]"
            }

    filename = "holisticsummarydata.csv"
    FileExist = 1
    if not path.exists(filename):
        FileExist = 0
    file = open(filename, "a", encoding="utf-8")
    if FileExist == 0:
        file.write("Account Number, Unifi TV, Segment Code, Plan Subscribe, Difficult Customer, Internet, Voice, Commitment Start/End Date, VVIP Customer, Customer Level, MSISDN, IMSI Number, Autopay Status, Source, Installation Address")

    acc_number = driver.find_element(by=By.XPATH, value=xpath["account_number"]).text
    unifi = driver.find_element(by=By.XPATH, value=xpath["unifi_tv"]).text
    segment = driver.find_element(by=By.XPATH, value=xpath["segment_code"]).text
    plan = driver.find_element(by=By.XPATH, value=xpath["plan_subscribe"]).text
    diff_cust = driver.find_element(by=By.XPATH, value=xpath["difficult_customer"]).text
    internet = driver.find_element(by=By.XPATH, value=xpath["internet1"]).text
    voice = driver.find_element(by=By.XPATH, value=xpath["voice1"]).text
    commitment_start_end_date = driver.find_element(by=By.XPATH, value=xpath["commitment"]).text
    vvip = driver.find_element(by=By.XPATH, value=xpath["vvip_customer"]).text
    cust_level = driver.find_element(by=By.XPATH, value=xpath["customer_level"]).text
    msisdn = driver.find_element(by=By.XPATH, value=xpath["msisdn1"]).text
    imsi = driver.find_element(by=By.XPATH, value=xpath["imsi_number"]).text
    autopay = driver.find_element(by=By.XPATH, value=xpath["autopay_status"]).text
    source = driver.find_element(by=By.XPATH, value=xpath["source1"]).text
    install_add = driver.find_element(by=By.XPATH, value=xpath["installation_address"]).text

    time.sleep(20)
    file.write("\n\"" + acc_number + "\",\"" + unifi + "\",\"" + segment + "\",\"" + plan + "\",\"" + diff_cust +
               "\",\"" + internet + "\",\"" + voice + "\",\"" + commitment_start_end_date + "\",\"" + vvip +
               "\",\"" + cust_level + "\",\"" + msisdn + "\",\"" + imsi + "\",\"" + autopay + "\",\"" + source +
               "\",\"" + install_add + "\"")

def test_collectdata_profile():

               #CONTACT INFO SECTION
    xpath = { "customer_name" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-profile/div/div/div/div/p[3]",
              "new_nric" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-profile/div/div/div/div/p[5]",
              "contact_number" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-profile/div/div/div/div/p[7]",
              "installation_address" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-profile/div/div/div/div/p[9]",
              "corresponding_address" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-profile/div/div/div/div/p[11]",
              "account_number" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-profile/div/div/div/div/p[13]",
              "state1" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-profile/div/div/div/div/p[15]",
              "region1" : "/html/body/app-root/app-main/body/div/div/main/app-content/app-profile/div/div/div/div/p[17]"
            }

    profile = driver.find_element_by_xpath("//a[text()=' Profile ']").click()
    time.sleep(15)

    filename = "profiledata.csv"
    FileExist = 1
    if not path.exists(filename):
        FileExist = 0
    file = open(filename, "a", encoding="utf-8")
    if FileExist == 0:
        file.write("Name, New NRIC, Contact Number, Account Number, State, Region")

    name = driver.find_element(by=By.XPATH, value=xpath["customer_name"]).text
    nric = driver.find_element(by=By.XPATH, value=xpath["new_nric"]).text
    contact = driver.find_element(by=By.XPATH, value=xpath["contact_number"]).text
    #install_add = driver.find_element(by=By.XPATH, value=xpath["installation_address"]).text
    #correspond_add = driver.find_element(by=By.XPATH, value=xpath["corresponding_address"]).text
    acc_number = driver.find_element(by=By.XPATH, value=xpath["account_number"]).text
    state = driver.find_element(by=By.XPATH, value=xpath["state1"]).text
    region = driver.find_element(by=By.XPATH, value=xpath["region1"]).text

    time.sleep(20)
    file.write("\n\"" + name + "\",\"" + nric + "\",\"" + contact + "\",\"" + acc_number + "\",\"" + state + "\",\"" + region +
               "\"")
